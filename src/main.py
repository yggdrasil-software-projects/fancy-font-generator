# Fancy font generator that takes in user input and outputs it in the specified font
# When the program runs:
#   > User will be asked to choose a font (1 of 5 presets)
#       > If wrong input: the script'll loop the "font-choosing stage"
#       > If correct input: the loop'll break and the script continues
#   > User will be asked for string of text to convert
#   > The script'll convert the text and output it

# 65 lines of real code

# To do:
#   > Can possibly turn font assignments into another function (later; not MVP)
#   > Polish code

# entire script as a function
def main():
    def generate():
        try:
            # dicts for fonts and numerical assignments later
            l_fancy_low = {}
            l_fancy_up = {}

            # splitting up the font strings for loop iteration and dict key assignment
            split_fancy_low = [*fancy_low_string]
            split_fancy_up = [*fancy_up_string]

            # lowercase fancy font dict key assignment
            number = 96
            for c in split_fancy_low:
                number = number + 1
                letter = chr(number)
                l_fancy_low[letter] = c

            # uppercase fancy font dict key assignment
            number = 64
            for c in split_fancy_up:
                number = number + 1
                letter = chr(number)
                l_fancy_up[letter] = c

            output = ""

            # user input and main font reprinting script
            sentence = input("Input your text here:\n> ")
            characters = [*sentence]
            for c in characters:
                if c.isupper():
                    output = output + l_fancy_up[c]
                elif c.islower():
                    output = output + l_fancy_low[c]
                else:
                    output = output + c

            # self-explanatory output... end of script
            print(f"Fancy-fied text:\n> {output}")

        except Exception as e:
            input(e)

        except KeyboardInterrupt:
            input("\nDo not use keyboard interruptions. Shame, shame, shame.")

    try:
        fancy_low_string = ""
        fancy_up_string = ""
        flag = True
        # Font-choosing stage
        while flag:
            font = input("Select a following font:\n1). 𝔅𝔩𝔞𝔠𝔨𝔩𝔢𝔱𝔱𝔢𝔯 (Blackletter)\n2). 𝕭𝖑𝖆𝖈𝖐𝖑𝖊𝖙𝖙𝖊𝖗 𝕭𝖔𝖑𝖉 (Blackletter Bold)\n3). 𝒞𝓊𝓇𝓈𝒾𝓋𝑒 (Cursive)\n4). 𝓒𝓾𝓻𝓼𝓲𝓿𝓮 𝓑𝓸𝓵𝓭 (Cursive Bold)\n5). 𝕋𝕨𝕒𝕚𝕟𝕝𝕚𝕟𝕖𝕕 (Twainlined)\n(Reply with the number indicated prefixing the font name.)\n> ")

            match font:
                case "1":
                    # Blackletter string assignment
                    fancy_low_string = "𝔞𝔟𝔠𝔡𝔢𝔣𝔤𝔥𝔦𝔧𝔨𝔩𝔪𝔫𝔬𝔭𝔮𝔯𝔰𝔱𝔲𝔳𝔴𝔵𝔶𝔷"
                    fancy_up_string = "𝔄𝔅ℭ𝔇𝔈𝔉𝔊ℌℑ𝔍𝔎𝔏𝔐𝔑𝔒𝔓𝔔ℜ𝔖𝔗𝔘𝔙𝔚𝔛𝔜ℨ"
                    flag = False
                case "2":
                    # blackletter bold string assignment
                    fancy_low_string = "𝖆𝖇𝖈𝖉𝖊𝖋𝖌𝖍𝖎𝖏𝖐𝖑𝖒𝖓𝖔𝖕𝖖𝖗𝖘𝖙𝖚𝖛𝖜𝖝𝖞𝖟"
                    fancy_up_string = "𝕬𝕭𝕮𝕯𝕰𝕱𝕲𝕳𝕴𝕵𝕶𝕷𝕸𝕹𝕺𝕻𝕼𝕽𝕾𝕿𝖀𝖁𝖂𝖃𝖄𝖅"
                    flag = False
                case "3":
                    # cursive string assignment
                    fancy_low_string = "𝒶𝒷𝒸𝒹𝑒𝒻𝑔𝒽𝒾𝒿𝓀𝓁𝓂𝓃𝑜𝓅𝓆𝓇𝓈𝓉𝓊𝓋𝓌𝓍𝓎𝓏"
                    fancy_up_string = "𝒜𝐵𝒞𝒟𝐸𝐹𝒢𝐻𝐼𝒥𝒦𝐿𝑀𝒩𝒪𝒫𝒬𝑅𝒮𝒯𝒰𝒱𝒲𝒳𝒴𝒵"
                    flag = False
                case "4":
                    # cursive bold string assignment
                    fancy_low_string = "𝓪𝓫𝓬𝓭𝓮𝓯𝓰𝓱𝓲𝓳𝓴𝓵𝓶𝓷𝓸𝓹𝓺𝓻𝓼𝓽𝓾𝓿𝔀𝔁𝔂𝔃"
                    fancy_up_string = "𝓐𝓑𝓒𝓓𝓔𝓕𝓖𝓗𝓘𝓙𝓚𝓛𝓜𝓝𝓞𝓟𝓠𝓡𝓢𝓣𝓤𝓥𝓦𝓧𝓨𝓩"
                    flag = False
                case "5":
                    # twainlined string assignment
                    fancy_low_string = "𝕒𝕓𝕔𝕕𝕖𝕗𝕘𝕙𝕚𝕛𝕜𝕝𝕞𝕟𝕠𝕡𝕢𝕣𝕤𝕥𝕦𝕧𝕨𝕩𝕪𝕫"
                    fancy_up_string = "𝔸𝔹ℂ𝔻𝔼𝔽𝔾ℍ𝕀𝕁𝕂𝕃𝕄ℕ𝕆ℙℚℝ𝕊𝕋𝕌𝕍𝕎𝕏𝕐ℤ"
                    flag = False

        generate()

    except Exception as e:
        input(e)

    except KeyboardInterrupt:
        input("\nDo not use keyboard interruptions. Shame, shame, shame.")

main()