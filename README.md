# Fancy Font Generator
Work in progress.
Full readme coming soon.

---

## Setup:
###### 1. Download a zip file of the [main branch](https://gitlab.com/yggdrasil-software-projects/weather-app/-/tree/main?ref_type=heads).
###### 2. A download button is located at the middle-right of the [main page](https://gitlab.com/yggdrasil-software-projects/weather-app/-/tree/main?ref_type=heads).
###### 3. Click it. Under "Download source code", click on "zip" and it'll download the current branch of the repository as a zipped file.
###### 4. Unzip the zipped file; you can put the extracted directory anywhere.
###### 5. Navigate into the extracted folder and go inside of the "dist" directory.
###### HOWEVER: Read the below warning before executing _anything_.

---

## Warning:
### When this program is accessed by double-clicking (or simply run through) the executable inside the dist directory, or the Python file inside the src directory, the command line will be unable to display the new fonts properly.
### However, the encoding is still true and valid, and you can simply copy and paste the new font without issue.
### Trust the process.